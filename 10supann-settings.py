import os

A2_IDP_SAML2_ENABLE = True
A2_PROFILE_CAN_CHANGE_EMAIL = False
A2_PROFILE_CAN_EDIT_PROFILE = False
A2_CAN_RESET_PASSWORD = False
REGISTRATION_OPEN = False
A2_REGISTRATION_CAN_CHANGE_PASSWORD = False
A2_REGISTRATION_CAN_DELETE_ACCOUNT = False

with open('/etc/authentic2/cert.pem') as fd:
    SAML_SIGNATURE_PUBLIC_KEY = fd.read()

with open('/etc/authentic2/key.pem') as fd:
    SAML_SIGNATURE_PRIVATE_KEY = fd.read()

SUPANN_LDAP_EXTRA_ATTRIBUTES = list(filter(None, os.environ.get('SUPANN_LDAP_EXTRA_ATTRIBUTES', '').split()))

LDAP_AUTH_SETTINGS = [
    {
        'url': os.environ['SUPANN_LDAP_URL'],
        'user_filter': '(&(|(mail=%s)(supannAutreMail=%s)(supannAliasLogin=%s)(uid=%s))(objectClass=supannPerson))',
        'basedn': os.environ['SUPANN_LDAP_BASE_DN'],
        'binddn': os.environ.get('SUPANN_LDAP_BINDDN'),
        'bindpw': os.environ.get('SUPANN_LDAP_BINDPW'),
        'groupsu': 'cn=admin,ou=groups,%s' % os.environ['SUPANN_LDAP_BASE_DN'],
        'groupstaff': 'cn=admin,ou=groups,%s' % os.environ['SUPANN_LDAP_BASE_DN'],
        'username_template': '{uid[0]}',
        'external_id_tuples': (('uid',), ('dn:noquote',), ),
        'lookups': ('external_id',),
        'update_username': False,
        'use_tls': os.environ.get('USE_TLS') != '0',
        'attributes': [
            'audio',
            'businessCategory',
            'carLicense',
            'cn',
            'departmentNumber',
            'description',
            'destinationIndicator',
            'displayName',
            'eduPersonAffiliation',
            'eduPersonAssurance',
            'eduPersonEntitlement',
            'eduPersonNickname',
            'eduPersonOrgDN',
            'eduPersonOrgUnitDN',
            'eduPersonPrimaryAffiliation',
            'eduPersonPrimaryOrgUnitDN',
            'eduPersonPrincipalName',
            'eduPersonScopedAffiliation',
            'em',
            'employeeType',
            'fax',
            'ferredLanguage',
            'givenName',
            'homePhone',
            'homePostalAddress',
            'ini',
            'internationaliSDNNumber',
            'jpegPhoto',
            'l',
            'labeledURI',
            'mail',
            'mailForwardingAddress',
            'manager',
            'mobile',
            'o',
            'ou',
            'pager',
            'photo',
            'physicalDeliveryOfficeName',
            'ployeeNumber',
            'postalAddress',
            'postalCode',
            'postOfficeBox',
            'pre',
            'preferredDeliveryMethod',
            'preferredLanguage',
            'registeredAddress',
            'roomNumber',
            'secretary',
            'seeAlso',
            'sn',
            'st',
            'street',
            'supannActivite',
            'supannAliasLogin',
            'supannAutreMail',
            'supannAutreTelephone',
            'supannCivilite',
            'supannCodeINE',
            'supannEmpCorps',
            'supannEmpId',
            'supannEntiteAffectation',
            'supannEntiteAffectationPrincipale',
            'supannEtablissement',
            'supannEtuAnneeInscription',
            'supannEtuCursusAnnee',
            'supannEtuDiplome',
            'supannEtuElementPedagogique',
            'supannEtuEtape',
            'supannEtuId',
            'supannEtuInscription',
            'supannEtuRegimeInscription',
            'supannEtuSecteurDisciplinaire',
            'supannEtuTypeDiplome',
            'supannListeRouge',
            'supannMailPerso',
            'supannParrainDN',
            'supannRefId',
            'supannRoleEntite',
            'supannRoleGenerique',
            'supannTypeEntiteAffectation',
            'telephoneNumber',
            'teletexTerminalIdentifier',
            'telexNumber',
            'tials',
            'title',
            'uid',
            'userCertificate',
            'userPassword',
            'userPKCS12',
            'userSMIMECertificate',
            'x121Address',
            'x500uniqueIdentifier',
        ] + SUPANN_LDAP_EXTRA_ATTRIBUTES,
        'attribute_mappings': (('mail', 'email'),),
        'mandatory_attributes_values': {
            # edugain support
            'schacHomeOrganization': [os.environ['EDUGAIN_SCHAC_HOME_ORGANIZATION']],
            'schacHomeOrganizationtype': [os.environ['EDUGAIN_SCHAC_HOME_ORGANIZATION_TYPE']],
        },
    }
]
AUTHENTICATION_BACKENDS = (
    'authentic2.backends.ldap_backend.LDAPBackend',
    'django_rbac.backends.DjangoRBACBackend',
)

ATTRIBUTE_SOURCES = [
    ('function', {
        'name': 'entity_id',
        'dependencies': ['service'],
        'function': lambda ctx: getattr(ctx.get('service'), 'entity_id', ''),
    }),
    ('computed_targeted_id', {
        'name': 'edupersontargetedid',
        'label': 'eduPersonTargetedId',
        'source_attributes': ['entity_id', 'django_user_username'],
        'salt': 'edupersontargetedid',
    })
]

# bypass authentic2 warning: "DEFAULT_FROM_EMAIL must be customized"
DEFAULT_FROM_EMAIL = 'root@localhost'

SESSION_COOKIE_AGE = int(os.environ.get('SESSION_COOKIE_AGE', 10 * 3600))

if os.environ.get('DEBUG') == '1':
    DEBUG = True
    for logger in LOGGING['loggers'].values():
        logger['level'] = 'DEBUG'
