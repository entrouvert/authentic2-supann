NAME=authentic2-supann
VERSION=`git describe | tr - . | cut -c2-`
FULLNAME=$(NAME)-$(VERSION)

.PHONY: dist-bzip2

all:

install:

uninstall:

dist-bzip2:
	rm -rf build dist
	mkdir -p build/$(FULLNAME) sdist
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done
	cd build && tar cfj ../sdist/$(FULLNAME).tar.bz2 .
	rm -rf build

clean:
	rm -rf sdist build

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(FULLNAME))

package:
	make dist-bzip2
	cd sdist; mv $(FULLNAME).tar.bz2 $(NAME)_$(VERSION).orig.tar.bz2; tar xvjf $(NAME)_$(VERSION).orig.tar.bz2
	cp -R debian sdist/$(FULLNAME)/
	cd sdist/$(FULLNAME); dpkg-buildpackage -uc -us
	rm -rf sdist/$(FULLNAME)


