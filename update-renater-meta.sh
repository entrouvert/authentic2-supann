#!/bin/bash

set -e

DEFAULT="/etc/default/authentic2"
BASEDIR=`dirname $0`
METADATA_TMP=`tempfile`
CERTIFICATE_TMP=`tempfile`
FIXTURE_TMP=`tempfile --suffix=.json`
TIMEOUT=30

function cleanup {
	rm -f $METADATA_TMP $CERTIFICATE_TMP $FIXTURE_TMP
}

trap "cleanup" EXIT

if [ -f  ]; then
	. /etc/default/authentic2
else
	. $BASEDIR/`basename $DEFAULT`
fi

if ! wget --tries=3 --timeout=$TIMEOUT --quiet $RENATER_METADATA -O$METADATA_TMP; then
	echo ERROR: unable to retrieve metadata from $RENATER_METADATA
	exit 1
fi

if ! wget --tries=3 --timeout=$TIMEOUT --quiet $RENATER_CERTIFICATE -O$CERTIFICATE_TMP; then
	echo ERROR: unable to retrieve Renater metadata signing certificate from $RENATER_CERTIFICATE
	exit 1
fi

if ! xmllint $METADATA_TMP >/dev/null; then
	echo ERROR: xmllint failed on renater metadata
	exit 1
fi

# Verify metadata signature
if ! xmlsec1 --verify --id-attr:ID EntitiesDescriptor --pubkey-cert-pem $CERTIFICATE_TMP --enabled-key-data key-name $METADATA_TMP 2>/dev/null >/dev/null; then
	echo ERROR: unable to validate signature on $RENATER_METADATA
	exit 1
fi

if [ "$ALLOW_SLO" = "0" ]; then
	SLO_SUPPORT=false
else
	SLO_SUPPORT=true
fi


# Build fixture
cat <<EOF >$FIXTURE_TMP
[
{
    "model": "saml.spoptionsidppolicy",
    "fields" : {
        "accept_slo" : $SLO_SUPPORT,
        "accepted_name_id_format" : "transient,persistent",
        "ask_user_consent" : false,
        "authn_request_signed" : false,
        "default_name_id_format" : "transient",
        "enabled" : true,
        "encrypt_assertion" : false,
        "encrypt_nameid" : false,
        "federation_mode" : 0,
        "forward_slo" : true,
        "http_method_for_slo_request" : 4,
        "idp_initiated_sso" : $SLO_SUPPORT,
        "iframe_logout_timeout" : 300,
        "name" : "Default",
        "needs_iframe_logout" : false,
        "prefered_assertion_consumer_binding" : "meta"
      }
}]
EOF

# Load fixture
/usr/bin/authentic2-manage loaddata -v0 $FIXTURE_TMP

# Load metadataas
/usr/bin/authentic2-manage sync-metadata --source=renater --sp -v1 $METADATA_TMP
