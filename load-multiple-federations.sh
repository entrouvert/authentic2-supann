#!/bin/bash

function load_meta() {
set -e

DEFAULT="/etc/default/authentic2"
BASEDIR=`dirname $0`
METADATA_TMP=`tempfile`
FIXTURE_TMP=`tempfile --suffix=.json`
TIMEOUT=30

trap "rm -f $METADATA_TMP $FIXTURE_TMP" EXIT

if [ -f  ]; then
	. /etc/default/authentic2
else
	. $BASEDIR/`basename $DEFAULT`
fi

if ! wget --tries=3 --timeout=$TIMEOUT --quiet $METADATA -O$METADATA_TMP; then
	echo ERROR: unable to retrieve metadata from $METADATA
	exit 1
fi

if ! xmllint $METADATA_TMP >/dev/null; then
	echo ERROR: xmllint failed on $SOURCE metadata
	exit 1
fi

if [ "$ALLOW_SLO" = "0" ]; then
	SLO_SUPPORT=false
else
	SLO_SUPPORT=true
fi


# Build fixture
cat <<EOF >$FIXTURE_TMP
[
{
    "model": "saml.spoptionsidppolicy",
    "fields" : {
        "accept_slo" : $SLO_SUPPORT,
        "accepted_name_id_format" : "transient,persistent",
        "ask_user_consent" : false,
        "authn_request_signed" : false,
        "default_name_id_format" : "transient",
        "enabled" : true,
        "encrypt_assertion" : false,
        "encrypt_nameid" : false,
        "federation_mode" : 0,
        "forward_slo" : true,
        "http_method_for_slo_request" : 4,
        "idp_initiated_sso" : $SLO_SUPPORT,
        "iframe_logout_timeout" : 300,
        "name" : "Default",
        "needs_iframe_logout" : false,
        "prefered_assertion_consumer_binding" : "meta"
      }
}]
EOF

chmod +r $FIXTURE_TMP
chmod +r $METADATA_TMP

# Load fixture
/usr/bin/authentic2-manage loaddata -v0 $FIXTURE_TMP

# Load metadata
/usr/bin/authentic2-manage sync-metadata --source=$SOURCE --sp -v1 $METADATA_TMP

rm -f $METADATA_TMP $FIXTURE_TMP
}

for config in /etc/authentic2/federations.d/*.sh; do
	METADATA=
	SOURCE=
	. $config
	load_meta "$METADATA" "$SOURCE"
done
